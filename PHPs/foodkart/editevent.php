<?php
/**
 * Created by PhpStorm.
 * User: Udara Indeewarie
 * Date: 07-Nov-16
 * Time: 3:36 PM
 */


header ('Access-Control-Allow-Origin: *');
header ('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header ('Access-Control-Allow-Headers: Content-Type,x-prototype-version,x-requested-with');

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "youandme";
// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$appID = $_GET['id'];
$query = "SELECT id,ename,edate,starttime,loc,endtime FROM events WHERE id = $appID";
$result = $conn->query($query) or die($conn->error.__LINE__);



$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"Name":"'  . $rs["ename"] . '",';
    $outp .= '"Starttime":"'  . $rs["starttime"] . '",';
    $outp .= '"Endtime":"'  . $rs["endtime"] . '",';

    $outp .= '"Location":"'  . $rs["loc"] . '",';
    $outp .= '"ID":"'  . $rs["id"] . '",';
    $outp .= '"Date":"'. $rs["edate"]     . '"}';
}
$outp ='{"records":['.$outp.']}';
$conn->close();

echo($outp);

?>
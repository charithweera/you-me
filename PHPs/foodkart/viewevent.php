<?php
/**
 * Created by PhpStorm.
 * User: Udara Indeewarie
 * Date: 07-Nov-16
 * Time: 3:22 PM
 */


header ('Access-Control-Allow-Origin: *');
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "youandme";
// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$query = "SELECT id,ename,edate FROM events";
$result = $conn->query($query) or die($conn->error.__LINE__);



$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"Name":"'  . $rs["ename"] . '",';
    $outp .= '"ID":"'  . $rs["id"] . '",';
    $outp .= '"Date":"'. $rs["edate"]     . '"}';
}
$outp ='{"records":['.$outp.']}';
$conn->close();

echo($outp);

?>

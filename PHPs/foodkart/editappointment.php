<?php
/**
 * Created by PhpStorm.
 * User: Udara Indeewarie
 * Date: 18-Oct-16
 * Time: 8:21 PM
 */

header ('Access-Control-Allow-Origin: *');
header ('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header ('Access-Control-Allow-Headers: Content-Type,x-prototype-version,x-requested-with');

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "youandme";
// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$appID = $_GET['id'];
$query = "SELECT aid,aname,adate,astrttime,aloc,aendtime,ades FROM appointments WHERE aid = $appID";
$result = $conn->query($query) or die($conn->error.__LINE__);



$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"Name":"'  . $rs["aname"] . '",';
    $outp .= '"Starttime":"'  . $rs["astrttime"] . '",';
    $outp .= '"Endtime":"'  . $rs["aendtime"] . '",';
    $outp .= '"Location":"'  . $rs["aloc"] . '",';
    $outp .= '"Description":"'  . $rs["ades"] . '",';
    $outp .= '"ID":"'  . $rs["aid"] . '",';
    $outp .= '"Date":"'. $rs["adate"]     . '"}';
}
$outp ='{"records":['.$outp.']}';
$conn->close();

echo($outp);

?>
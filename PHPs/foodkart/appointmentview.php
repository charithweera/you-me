<?php
/**
 * Created by PhpStorm.
 * User: Udara Indeewarie
 * Date: 18-Oct-16
 * Time: 11:28 AM
 */

header ('Access-Control-Allow-Origin: *');
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "youandme";
// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$query = "SELECT aid,aname,adate FROM appointments";
$result = $conn->query($query) or die($conn->error.__LINE__);



$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"Name":"'  . $rs["aname"] . '",';
    $outp .= '"ID":"'  . $rs["aid"] . '",';
    $outp .= '"Date":"'. $rs["adate"]     . '"}';
}
$outp ='{"records":['.$outp.']}';
$conn->close();

echo($outp);

?>

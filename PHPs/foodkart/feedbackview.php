<?php
/**
 * Created by PhpStorm.
 * User: Udara Indeewarie
 * Date: 21-Oct-16
 * Time: 5:23 PM
 */
header ('Access-Control-Allow-Origin: *');
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "youandme";
// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$query = "SELECT feedback FROM feedback";
$result = $conn->query($query) or die($conn->error.__LINE__);



$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    if ($outp != "") {$outp .= ",";}
    $outp .= '{"Feedback":"'  . $rs["feedback"] . '"}';
}
$outp ='{"records":['.$outp.']}';
$conn->close();

echo($outp);

?>
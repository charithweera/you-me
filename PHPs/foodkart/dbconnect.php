<?php
/**
 * Created by PhpStorm.
 * User: Udara Indeewarie
 * Date: 18-Oct-16
 * Time: 2:52 PM
 */

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "youandme";
// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";
?>
angular.module('app.controllers', ['ionic', 'ngCordova'])

//angular.module('starter', ['ionic', 'ngCordova'])
  
.controller('menuCtrl', function($scope,$http,sharedCartService,sharedFilterService) {


	//put cart after menu
	var cart = sharedCartService.cart;
	
	
	
	$scope.slide_items=[    {"p_id":"1",
						 "p_name":"Hero",
						 "p_description":"Product Description",
						 "p_image_id":"Hero",
						 "p_price":"183"},
						
						{"p_id":"2",
						"p_name":"WEDDING",
						"p_description":"Product Description",
						"p_image_id":"WEDDING",
						"p_price":"171"},
						
						{"p_id":"3",
						"p_name":"model",
						"p_description":"Product Description",
						"p_image_id":"model",
						"p_price":"167"}
				   ];
					   
					   
	
	
	$scope.noMoreItemsAvailable = false; // lazy load list
	
	

				
  	//loads the menu----onload event
	$scope.$on('$stateChangeSuccess', function() {
		$scope.loadMore();  //Added Infine Scroll
	});
	 
	// Loadmore() called inorder to load the list 
	$scope.loadMore = function() {
		
			str=sharedFilterService.getUrl();
			$http.get(str).success(function (response){
				$scope.menu_items = response.records;
				$scope.hasmore=response.has_more;	//"has_more": 0	or number of items left
				$scope.$broadcast('scroll.infiniteScrollComplete');
			});	
			
			//more data can be loaded or not
			if ( $scope.hasmore == 0 ) {
			  $scope.noMoreItemsAvailable = true;
			}
	};
	 
	
	 //show product page
	$scope.showProductInfo=function (id,desc,img,name,price) {	 
		 sessionStorage.setItem('product_info_id', id);
		 sessionStorage.setItem('product_info_desc', desc);
		 sessionStorage.setItem('product_info_img', img);
		 sessionStorage.setItem('product_info_name', name);
		 sessionStorage.setItem('product_info_price', price);
		 window.location.href = "#/page13";
	 };

	 //add to cart function
	 $scope.addToCart=function(id,image,name,price){    
		cart.add(id,image,name,price,1);	
	 };	 
})
   
.controller('cartCtrl', function($scope,sharedCartService,$ionicPopup,$state) {
		
		//onload event-- to set the values
		$scope.$on('$stateChangeSuccess', function () {
			$scope.cart=sharedCartService.cart;
			$scope.total_qty=sharedCartService.total_qty;
			$scope.total_amount=sharedCartService.total_amount;		
		});
		
		//remove function
		$scope.removeFromCart=function(c_id){
			$scope.cart.drop(c_id);	
			$scope.total_qty=sharedCartService.total_qty;
			$scope.total_amount=sharedCartService.total_amount;
			
		};
		
		$scope.inc=function(c_id){
			$scope.cart.increment(c_id);
			$scope.total_qty=sharedCartService.total_qty;
			$scope.total_amount=sharedCartService.total_amount;
		};
		
		$scope.dec=function(c_id){
			$scope.cart.decrement(c_id);
			$scope.total_qty=sharedCartService.total_qty;
			$scope.total_amount=sharedCartService.total_amount;
		};
		
		$scope.checkout=function(){
			if($scope.total_amount>0){
				$state.go('checkOut');
			}
			else{
				var alertPopup = $ionicPopup.alert({
					title: 'No item in your Cart',
					template: 'Please add Some Items!'
				});
			}
		};

})
   
.controller('checkOutCtrl', function($scope) {
	$scope.loggedin=function(){
		if(sessionStorage.getItem('loggedin_id')==null){return 1;}
		else{
			$scope.loggedin_name= sessionStorage.getItem('loggedin_name');
			$scope.loggedin_id= sessionStorage.getItem('loggedin_id');
			$scope.loggedin_phone= sessionStorage.getItem('loggedin_phone');
			$scope.loggedin_address= sessionStorage.getItem('loggedin_address');
			$scope.loggedin_pincode= sessionStorage.getItem('loggedin_pincode');
			return 0;
		}
	};
	
	

})

.controller('indexCtrl', function($scope,sharedCartService) {
	//$scope.total = 10; 
})
	.controller('loginCtrl', function($scope,$http,$ionicPopup,$state,$ionicHistory) {
		$scope.user = {};  //declares the object user

		$scope.login = function() {
			str="http://localhost:8000/foodkart/user-details.php?e="+$scope.user.email+"&p="+$scope.user.password;
			$http.get(str)
				.success(function (response){   // if login request is Accepted

					// records is the 'server response array' variable name.
					$scope.user_details = response.records;  // copy response values to user-details object.

					//stores the data in the session. if the user is logged in, then there is no need to show login again.
					sessionStorage.setItem('loggedin_name', $scope.user_details.u_name);
					sessionStorage.setItem('loggedin_id', $scope.user_details.u_id );
					sessionStorage.setItem('loggedin_phone', $scope.user_details.ddu_phone);
					sessionStorage.setItem('loggedin_address', $scope.user_details.u_address);
					sessionStorage.setItem('loggedin_pincode', $scope.user_details.u_pincode);

					// remove the instance of login page, when user moves to profile page.
					// if you dont use it, you can get to login page, even if you are already logged in .
					$ionicHistory.nextViewOptions({
						disableAnimate: true,
						disableBack: true
					});

					//in my FoodKart App, it checks the page from where the user logs in.
					//if it is from the check out, then after login, the check out page will be shown.
					//else normal profile page will be shown

					lastView = $ionicHistory.backView();
					if(lastView.stateId=="checkOut"){ $state.go('checkOut', {}, {location: "replace", reload: true}); }
					else{$state.go('profile', {}, {location: "replace", reload: true});}


				}).error(function() {   						//if login failed
				var alertPopup = $ionicPopup.alert({
					title: 'Login failed!',
					template: 'Please check your credentials!'
				});
			});
		};

	})

	.controller('signupCtrl', function($scope,$http,$ionicPopup,$state,$ionicHistory) {

		$scope.signup=function(data){

			var link = 'http://localhost:8000/foodkart/signup.php';

			//using http post as we are passing password.
			$http.post(link, {n : data.name, un : data.username, ps : data.password , ph: data.phone , add : data.address , pin : data.pincode })
				.then(function (res){	 //if a response is recieved from the server.

					$scope.response = res.data.result; //contains Register Result

					//Shows the respective popup and removes back link
					if($scope.response.created=="1"){
						//no back option
						$ionicHistory.nextViewOptions({
							disableAnimate: true,
							disableBack: true
						});
						// the user is redirected to login page after sign up
						$state.go('login', {}, {location: "replace", reload: true});

						$scope.title="Account Created!";
						$scope.template="Your account has been successfully created!";

					}else if($scope.response.exists=="1"){
						$scope.title="Email Already exists";
						$scope.template="Please click forgot password if necessary";

					}else{
						$scope.title="Failed";
						$scope.template="Contact Our Technical Team";
					}

					var alertPopup = $ionicPopup.alert({
						title: $scope.title,
						template: $scope.template
					});


				});

		}
	})
.controller('filterByCtrl', function($scope,sharedFilterService) {

  $scope.Categories = [
    {id: 1, name: 'Traditional'},
    // {id: 2, name: 'Drinks'}
  ];
  
  $scope.getCategory = function(cat_list){
    categoryAdded = cat_list;
	var c_string=""; // will hold the category as string
	
	for(var i=0;i<categoryAdded.length;i++){ c_string+=(categoryAdded[i].id+"||"); }
	
	c_string = c_string.substr(0, c_string.length-2);
	sharedFilterService.category=c_string;
	window.location.href = "#/page1";
  };
	

})
   
.controller('sortByCtrl', function($scope,sharedFilterService) {
	$scope.sort=function(sort_by){
		sharedFilterService.sort=sort_by;
		console.log('sort',sort_by);		
		window.location.href = "#/page1";
	};
})
   
.controller('paymentCtrl', function($scope) {

})
   
.controller('profileCtrl', function($scope,$rootScope,$ionicHistory,$state) {

		$scope.loggedin_name= sessionStorage.getItem('loggedin_name');
		$scope.loggedin_id= sessionStorage.getItem('loggedin_id');
		$scope.loggedin_phone= sessionStorage.getItem('loggedin_phone');
		$scope.loggedin_address= sessionStorage.getItem('loggedin_address');
		$scope.loggedin_pincode= sessionStorage.getItem('loggedin_pincode');
		
		
		$scope.logout=function(){
				delete sessionStorage.loggedin_name;
				delete sessionStorage.loggedin_id;
				delete sessionStorage.loggedin_phone;
				delete sessionStorage.loggedin_address;
				delete sessionStorage.loggedin_pincode;
				
				console.log('Logoutctrl',sessionStorage.getItem('loggedin_id'));
				
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true
				});
				$state.go('menu', {}, {location: "replace", reload: true});
		};
})
   
.controller('myOrdersCtrl', function($scope) {

})
   
.controller('editProfileCtrl', function($scope) {

})
   
.controller('favoratesCtrl', function($scope) {

})

.controller('eventsPageCtrl', function($scope, $http, $state, $ionicPopup,$stateParams) {

	$scope.date = new Date();
	$scope.DisplayAdd = true;
	var idforapp = $stateParams.eID;
	console.log('Tapped!', $stateParams.eID);
	if ($stateParams.eID){

		$scope.DisplayAdd = false;
		$scope.DisplayUpdate = true;

		var id = $stateParams.eID;
		console.log('Tapped!', $stateParams.eID);
		getTask(); // Load all available tasks
		function getTask() {
			$http.get(
				"http://localhost:8012/phpfilessep2/editevent.php?id=" + id).success(function (response) {
				console.log('EDITRETRN!', response.records[0]);
				//$scope.appNames = response.records[0];



				var status=response.records;
				var appoint = [];

				appoint = response.records;
				if(status){
					$scope.data = {};
					console.log('EDITRETRN!', response.records[0]);
					$scope.data.eventname = appoint[0].Name;

					var a = moment(appoint[0].Date).format('L');
					console.log('EDITRETRN!', a);
					$scope.data.eventloc = appoint[0].Location;
					var b = moment(appoint[0].Starttime).format('LT');
					console.log('EDITRETRN!', b);
					var c = moment(appoint[0].Endtime).format('LT');
					console.log('EDITRETRN!', c);
					$scope.data.eventsrttime = new Date(appoint[0].Starttime);
					$scope.data.eventendtime = new Date(appoint[0].Endtime);
					$scope.data.eventdate = new Date(a);
					//$scope.data.appdes = appoint[0].Description;

					//window.location.href = 'http://localhost:8012/you-me/www/templates/appointmentHandeling';
					//$location.path('/appointmentHandeling.html?appname='+ appoint[0].Name );
				}

				else {
					var alertsomePopup = $ionicPopup.alert({
						title:'YOUandME',
						template:'Process Failed'

					});

				}



			});
		}
	}

		$scope.createevent=function(data){

			//get event vales
			$http.post(
				"http://localhost:8012/phpfilessep2/addevent.php?eventname=" + data.eventname +
					"&eventloc=" + data.eventloc + "&eventsrttime=" + data.eventsrttime + "&eventendtime=" + data.eventendtime + "&eventdate=" +
				data.eventdate).success(function(data) {
				var alertPopup = $ionicPopup.alert({
					title: 'Event Added'
				});
				window.location.reload('true');
			});

		}

	$scope.updateevent=function(data){

		console.log('Tapped!', data);
		//get event vales
		$http.post(
			"http://localhost:8012/phpfilessep2/updateevent.php?eventname=" + data.eventname +
			"&eventloc=" + data.eventloc + "&eventsrttime=" + data.eventsrttime + "&eventendtime=" + data.eventendtime + "&eventdate=" +
			data.eventdate + "&eid=" + idforapp).success(function(response) {

			console.log('Tapped!', response);
			var alertPopup = $ionicPopup.alert({
				title: 'Event Updated'
			});
		

			$scope.data = {};

			$scope.data.eventname = "";

			$scope.data.eventloc = "";

			$scope.data.eventsrttime = "";
			$scope.data.eventendtime = "";
			$scope.data.appdate = "";


			$scope.DisplayAdd = true;
			$scope.DisplayUpdate = false;
		});

	}
})


	.controller('CalendarCtrl', function ($scope, $cordovaCalendar) {

$cordovaCalendar.createEvent({
	title: 'Space Race',
	location: 'The Moon',
	notes: 'Bring sandwiches',
	startDate: new Date(2015, 0, 6, 18, 30, 0, 0, 0),
	endDate: new Date(2015, 1, 6, 12, 0, 0, 0, 0)
}).then(function (result) {
	// success
}, function (err) {
	// error
});
	})

	.controller('appointmentsPageCtrl', function($scope,$http, $state, $ionicPopup, $stateParams ,$ionicHistory) {

		$scope.date = new Date();

		$scope.DisplayAdd = true;
		var idforapp = $stateParams.appID;
		console.log('Tapped!', $stateParams.appID);
		if ($stateParams.appID){

			$scope.DisplayAdd = false;
			$scope.DisplayUpdate = true;

		var id = $stateParams.appID;
		console.log('Tapped!', $stateParams.appID);
		getTask(); // Load all available tasks
		function getTask() {
			$http.get(
				"http://localhost:8012/phpfilessep2/editappointment.php?id=" + id).success(function (response) {
				//console.log('EDITRETRN!', response.records[0]);
				//$scope.appNames = response.records[0];



				var status=response.records;
							var appoint = [];

							appoint = response.records;
							if(status){
								$scope.data = {};
								console.log('EDITRETRN!', response.records[0]);
								$scope.data.appname = appoint[0].Name;

								var a = moment(appoint[0].Date).format('L');
								console.log('EDITRETRN!', a);
								$scope.data.apploc = appoint[0].Location;
								var b = moment(appoint[0].Starttime).format('LT');
								console.log('EDITRETRN!', b);
								var c = moment(appoint[0].Endtime).format('LT');
								console.log('EDITRETRN!', c);
								$scope.data.appsrttime = new Date(appoint[0].Starttime);
								$scope.data.appendtime = new Date(appoint[0].Endtime);
								$scope.data.appdate = new Date(a);
								$scope.data.appdes = appoint[0].Description;

								//window.location.href = 'http://localhost:8012/you-me/www/templates/appointmentHandeling';
								//$location.path('/appointmentHandeling.html?appname='+ appoint[0].Name );
							}

							else {
								var alertsomePopup = $ionicPopup.alert({
									title:'YOUandME',
									template:'Process Failed'

								});

							}



			});
		}
	}
        //
        //
		// $scope.myGoBack = function() {
		// 	$ionicHistory.goBack();
		// 	$state.go('appointmentsviewPage',{},{reload:true});
		// 	window.location.reload('true');
		// };

		// $scope.DisplayUpdate = false;
		// $scope.DisplayAdd = true;
		$scope.createappointment=function(data){

			// console.log('lalalalalat!', data.appsrttime);
			// console.log('lalalalalat!', data.appendtime);
			// var strttm = moment(data.appsrttime).format('LT');
			// var endtm = moment(data.appendtime).format('LT');
			// console.log('lalalalalat!', strttm);
			// console.log('lalalalalat!', endtm);
			//  var specstrttime = '08:00 AM';
			// var ss = moment(data.appendtime).format('LT');
			//  if (strttm < specstrttime ){
			// 	console.log('BA KARANNA!', strttm);
			//  }
			//get event vales
			$http.post(
				"http://localhost:8012/phpfilessep2/appointment.php?appname=" + data.appname +
				"&apploc=" + data.apploc +  "&appdes=" + data.appdes +"&appsrttime=" + data.appsrttime + "&appendtime=" + data.appendtime + "&appdate=" +
				data.appdate).success(function(data) {

				console.log('RESULT FROM result!', data);
				console.log('Tapped!', $stateParams.appID);
				if(data) {
					var alertPopup = $ionicPopup.alert({
						title: 'Appointment Added'
					});

					alertPopup.then(function(result) {
					// console.log('Tapped!', data);
					// console.log('Tapped!', $stateParams.appID);
					if (!$stateParams.appID) {
						window.location.reload('true');
					}
					$scope.data = {};

					$scope.data.appname = "";
					console.log('Tapped!', $scope.data.appname);

					$scope.data.apploc = "";

					$scope.data.appsrttime = "";
					$scope.data.appendtime = "";
					$scope.data.appdate = "";
					$scope.data.appdes = "";
					$scope.DisplayAdd = true;
					$scope.DisplayUpdate = false;
					});
				}
			});


		}

		$scope.updateappointment=function(data){

			console.log('Tapped!', data);
			//get event vales
			$http.post(
				"http://localhost:8012/phpfilessep2/updateappointment.php?appname=" + data.appname +
				"&apploc=" + data.apploc +  "&appdes=" + data.appdes +"&appsrttime=" + data.appsrttime + "&appendtime=" + data.appendtime + "&appdate=" +
				data.appdate + "&appid=" + idforapp).success(function(response) {

				console.log('Tapped!', response);
				var alertPopup = $ionicPopup.alert({
					title: 'Appointment Updated'
				});
				//window.location.reload('true');
				//$state.go('appointmentsviewPage',{},{reload:true});
				//viewModel.data = {};
				//$scope.appadding.$setUntouched();
				//$scope.appadding.$setPristine();
				//$scope = $scope.$new(true);

				$scope.data = {};

				$scope.data.appname = "";

				$scope.data.apploc = "";

				$scope.data.appsrttime = "";
				$scope.data.appendtime = "";
				$scope.data.appdate = "";
				$scope.data.appdes = "";

				$scope.DisplayAdd = true;
				$scope.DisplayUpdate = false;
			});

		}
	})

	// .controller('appointmentsPageEditCtrl', function($scope,$http, $state, $ionicPopup , $routeParams) {
    //
	// 	var id = $routeParams.appID;
	// 	console.log('Tapped!', $routeParams.appID);
	// 	getTask(); // Load all available tasks
	// 	function getTask() {
	// 		$http.get(
	// 			"http://localhost:8012/phpfilessep2/editappointment.php?id="+id).success(function (response) {
	// 			console.log('Tapped!', response.data.records);
	// 			$scope.appNames = response.data.records;
    //
	// 		});
	// 	}
	// })


	.controller('appointmentsviewPageCtrl', function($scope,$http, $state, $ionicPopup, $location) {

				var uid=1;

				getTask(); // Load all available tasks
				function getTask(){
					$http.get(
						"http://localhost:8012/phpfilessep2/appointmentview.php").success(function(response){
						console.log('Tapped!', response.records);
						$scope.appNames = response.records;

					});
				};
		$scope.delete=function(data){
			var alertPopup = $ionicPopup.confirm({
				title: 'delete',
				template: 'Are you sure you want to delete this?'
			});

			alertPopup.then(function(result) {

				if(result) {
					console.log('Tapped!', result);
					$http.get('http://localhost:8012/phpfilessep2/deleteappointment.php?id='+data ).then(function(response){

						var status=response.statusText;
						console.log('Tapsdfsdped!', response.statusText);
						if(status = "OK"){

							var alertthePopup = $ionicPopup.alert({
								title:'YOUandME',
								template:'Event was deleted successfully'

							});
							window.location.reload('true');
						}

						else {
							var alertsomePopup = $ionicPopup.alert({
								title:'YOUandME',
								template:'Process Failed'

							});

						}
					});

				}
			});

		}

		$scope.edit=function(data) {

			// getTask(); // Load all available tasks
			// function getTask(){
			// 	$http.get(
			// 		"http://localhost:8012/phpfilessep2/editappointment.php?id='+data").success(function(response){
			// 		console.log('Tapped!', response.data.records);
			// 		$scope.appNames = response.data.records;
            //
			// 	});
			// };
			// var alertPopup = $ionicPopup.alert({
			// 	title: 'edit',
			// 	template: 'Are you sure you want to edit this?'
			// });


			// alertPopup.then(function(result) {
            //
			// 	if(result) {
			//
			// 		console.log('Tapped!', result);
			// 		//var absUrl = location.absUrl();
			// 		//console.log('Tapped!', absUrl);
			// 		$http.get('http://localhost:8012/phpfilessep2/editappointment.php?id='+data ).then(function(response){
            //
			// 			var status=response.data.records;
			// 			var appoint = [];
			// 			console.log('Tapsdfsdped!', response.data.records);
			// 			appoint = response.data.records;
			// 			if(status){
			// 				$scope.appname = appoint[0].Name;
			// 				$scope.apploc = appoint[0].Location;
			// 				$scope.appsrttime = appoint[0].Starttime;
			// 				$scope.appendtime = appoint[0].Endtime;
			// 				$scope.appdate = appoint[0].Date;
			// 				$scope.appdes = appoint[0].Description;
            //
			// 				//window.location.href = 'http://localhost:8012/you-me/www/templates/appointmentHandeling';
			// 				$location.path('/appointmentHandeling.html?appname='+ appoint[0].Name );
			// 			}
            //
			// 			else {
			// 				var alertsomePopup = $ionicPopup.alert({
			// 					title:'YOUandME',
			// 					template:'Process Failed'
            //
			// 				});
            //
			// 			}
			// 		});
            //
			// 	}
			// });
		}


	})

	.controller('feedbackviewPageCtrl', function($scope,$http, $state, $ionicPopup, $location) {
		getTask(); // Load all available tasks
		function getTask(){
			$http.get(
				"http://localhost:8012/phpfilessep2/feedbackview.php").success(function(response){
				//console.log('Tapped!', response.records);
				//console.log('Tapped!', response.records[0]);
				console.log('Tapped!', response.records);
				$scope.fedbak = response.records;

			});
		};

	})

	.controller('addfeedbackPageCtrl', function($scope,$http, $state, $ionicPopup, $location) {
		$scope.addfeedbk = function (data) {

			//get feedback vales
			$http.post(
				"http://localhost:8012/phpfilessep2/addfeedback.php?feedback=" + data.feedback
				).success(function (data) {
				var alertPopup = $ionicPopup.alert({
					title: 'Feedback Added'
				});

				alertPopup.then(function(result) {
				window.location.reload('true');
				$scope.data = {};

				$scope.data.feedback = "";
				});

			});


		}

	})


	.controller('productPageCtrl', function($scope) {

	//onload event
	angular.element(document).ready(function () {
		$scope.id= sessionStorage.getItem('product_info_id');
		$scope.desc= sessionStorage.getItem('product_info_desc');
		$scope.img= "img/"+ sessionStorage.getItem('product_info_img')+".jpg";
		$scope.name= sessionStorage.getItem('product_info_name');
		$scope.price= sessionStorage.getItem('product_info_price');
	});




})

	.controller('contactusCtrl', function($scope,$http, $state, $ionicPopup, $stateParams ,$ionicHistory) {

		$scope.sendcontactform=function(data){

			//get event vales
			$http.post(
				"http://localhost:8012/phpfilessep2/contactus.php?cname=" + data.cname +
				"&csub=" + data.csubject + "&cemail=" + data.cemail + "&cmsg=" + data.cmsg).success(function(data) {
				console.log('Tapped!', data);
				var alertPopup = $ionicPopup.alert({
					title: 'Mail Sent. Thank You for contacting us.'
				});
				alertPopup.then(function(result) {
				window.location.reload('true');
				});
			});

		}

	})

	.controller('eventviewCtrl', function($scope,$http, $state, $ionicPopup, $location) {

		var uid=1;

		getTask(); // Load all available tasks
		function getTask(){
			$http.get(
				"http://localhost:8012/phpfilessep2/viewevent.php").success(function(response){
				console.log('Tapped!', response.records);
				$scope.eventNames = response.records;

			});
		};
		$scope.delete=function(data){
			var alertPopup = $ionicPopup.confirm({
				title: 'delete',
				template: 'Are you sure you want to delete this?'
			});



			alertPopup.then(function(result) {

				if(result) {
					console.log('Tapped!', result);
					$http.get('http://localhost:8012/phpfilessep2/deleteevent.php?id='+data ).then(function(response){

						var status=response.statusText;
						console.log('Tapsdfsdped!', response.statusText);
						if(status = "OK"){

							var alertthePopup = $ionicPopup.alert({
								title:'YOUandME',
								template:'Event was deleted successfully'

							});
							window.location.reload('true');
						}

						else {
							var alertsomePopup = $ionicPopup.alert({
								title:'YOUandME',
								template:'Process Failed'

							});

						}
					});

				}
			});

		}


	})


 
